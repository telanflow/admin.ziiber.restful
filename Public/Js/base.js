var HTTP = {
	
	request:function( url, method, params, success, error, cache )
	{
		method = method || 'get';
        cache  = cache  || false;

		var query = {};
		query.url = url;
		query.cache = cache;
		query.method = method;

		if( method.toLowerCase() !== 'get' && params )
            query.data = params;

		query.success = function( data, status, xhr ){
			if( typeof(success) == 'function' )
				success( data, status, xhr );
		}

		query.error = function(xhr, status, message){
            if( typeof error === 'function' )
                error( xhr.responseJSON, message, status );
        };

		$.ajax( query );
	}
}