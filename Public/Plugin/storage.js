var Storage = function(){
    this.keeper = null;

    if( typeof window.localStorage === typeof undefined )
        this.keeper = Cookie;
    else
        this.keeper = window.localStorage;
};

Storage.prototype.setItem = function( key , value ){
    this.keeper.setItem(key, value);
};

Storage.prototype.getItem = function( key ){
    return this.keeper.getItem(key);
};

Storage.prototype.setJson = function( key , value ){
    this.keeper.setItem(key, JSON.stringify(value));
};

Storage.prototype.getJson = function( key ){
    return JSON.parse(this.keeper.getItem(key));
};

Storage.prototype.removeItem = function( key ){
    return this.keeper.removeItem(key);
};

Storage.prototype.clear = function(){
    this.keeper.clear();
};