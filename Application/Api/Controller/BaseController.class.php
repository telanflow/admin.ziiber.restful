<?php
namespace Api\Controller;
use Think\Controller;

class BaseController extends Controller
{
	protected $allowMethod    = array('get','post','put','delete','options');  # REST允许的请求类型列表
    protected $allowType      = array('html','xml','json'); # REST允许请求的资源类型列表
    protected $_params 		  = array(); # 参数
    protected $_headers       = array(); # 请求头信息
    protected $_header		  = array(); # 发送头信息
    protected $_session		  = null;    # session 信息

	# 主入口
	public function _initialize()
	{
		$this->_headers = $this->getHeaders();
		if( IS_GET )
			$this->_params = I('get.');
		if( IS_POST )
			$this->_params = I('post.');
		if( IS_PUT )
			$this->_params = I('put.');


		if( !(CONTROLLER_NAME == 'Auth') )
		{
			if( I('server.REQUEST_METHOD', false, 'strtolower') != 'options' )
			{
				if( !$this->checkAccesstoken() )
		            $this->ajaxResponse( 403, '没有检测到token或当前token已过期' );
			}
			else
				$this->ajaxResponse( 0, 'success', '' );
		}

        if( session('?member') )
    		$this->_session = session('member');
	}

	# 是否存在参数
	protected function hasParams( $key )
	{
		if( isset($this->_params[$key]) )
            return true;
        else
            return false;
	}

	# 检查缺少的参数
	protected function checkParams( $keys = array() )
	{
        if( is_array($keys) )
        {
            foreach ($keys as $key)
            {
                if( !$this->hasParams($key) )
                {
                    $this->lackofParams( $key );
                    return false;
                }
            }
        }
        else
            if( !$this->hasParams($keys) )
            {
                $this->lackofParams( $keys );
                return false;
            }

        return true;
	}

	# 缺少字段的返回
    protected function lackofParams( $key )
    {
        $this->ajaxResponse( 500, 'invalid ' . $key, array() );
    }

	# ajax返回
	protected function ajaxResponse( $code, $message, $value = array(), $type = 'json' )
	{
		if( in_array( $code, array( 400, 404, 403, 500, 502) ) )
			$this->setHeader( $code );
		else
			$this->setHeader( 200 );

		# 添加 Header
		$this->setHeaderItem( 'x-powered-by', 'ziiber.me' );
        $this->setHeaderItem( 'x-api-version', '1.0.1' );
        $this->setHeaderItem( 'X-Frame-Options', 'deny' );
        $this->setHeaderItem( 'X-Xss-Protection', '1; mode=block' );
		$this->setHeaderItem( 'X-Content-Type-Options', 'nosniff' );

        # CORS
        # D('Host')->getHost()
        $this->setHeaderItem( 'Access-Control-Allow-Origin', '*' );
        $this->setHeaderItem( 'Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS' );
        $this->setHeaderItem( 'Access-Control-Allow-Headers', 'Origin, appid, secret, token, X-Requested-With, Content-Type, Accept' );
		
        # 把 Header 设置到Http协议上
		$this->sendHeader();

        $ret = array( 'code' => $code, 'message' => $message, 'value' => $value );
		$this->ajaxReturn( $ret, $type );
	}

	# 获取Header头信息
	protected function getHeaders()
	{
		$server = I('server.');
		$header = array();

		foreach ($server as $key => $value)
		{
			if( 'HTTP_' === substr( $key, 0, 5 ) )
			{
				$str = substr( $key, 5 );
				$str = strtolower($str);
				$header[$str] = $value;
			}
		}

		return $header;
	}

	# Header是否存在
	protected function hasHeaders( $key )
	{
		if( !isset($this->_headers[$key]) or empty($this->_headers[$key]) )
            return false;
        else
            return true;
	}

	# 检查缺少的Headers
	protected function checkHeaders( $keys = array() )
	{
        if( is_array($keys) )
        {
            foreach ($keys as $key)
            {
                if( !$this->hasHeaders($key) )
                {
                    $this->lackofHeaders( $key );
                    return false;
                }
            }
        }
        else
        {
            if( !$this->hasHeaders($keys) )
            {
                $this->lackofHeaders( $keys );
                return false;
            }
        }

        return true;
	}

	# 缺少Header的返回
    protected function lackofHeaders( $key )
    {
    	# Illegal request
        $this->ajaxResponse( 500, 'invalid headers ' . $key, array() );
    }

    # 非法请求返回
    protected function illegalParams( $key = 'Illegal request' )
    {
        $this->ajaxResponse( 403, $key, '' );
    }

	# 设置http返回状态
	protected function sendHeader()
	{
		foreach ($this->_header as $value)
			header( $value );
	}

	# 添加header 状态到 $this->_header
	protected function setHeader( $code )
	{
		$status = array(
			// Informational 1xx
			100 => 'Continue',
			101 => 'Switching Protocols',
			// Success 2xx
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			// Redirection 3xx
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Moved Temporarily ',  // 1.1
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			// 306 is deprecated but reserved
			307 => 'Temporary Redirect',
			// Client Error 4xx
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			// Server Error 5xx
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
			509 => 'Bandwidth Limit Exceeded'
		);
		
		if( array_key_exists( $code, $status ) )
			$this->_header[] = 'HTTP/1.1 '.$code.' '.$status[$code];
	}

	# 添加Header
	protected function setHeaderItem( $key, $value )
	{
		$this->_header[] = ucwords($key) . ': ' . $value;
	}
	
    # 检查请求的token是否存在
    protected function checkAccesstoken()
    {
        $params = $this->_headers;

        if( $params['token'] == null )
            return false;

		session('[destroy]');
		session( array( 'id'=>$params['token'], 'name'=>strtolower($params['token']), 'expire'=>3600 ) );
		session('[start]');
        # 检测token是否和appid对应
        if ( !session('?appid') ) 
            return false;
        else if ( session('appid') !== $params['appid'] ) 
            return false;
        # 检测token是否过期
        if ( !session('?expires') ) 
            return false;
        else if ( session('expires') < time() ) 
            return false;

        return true;
    }

}