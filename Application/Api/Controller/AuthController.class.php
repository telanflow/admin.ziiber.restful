<?php
namespace Api\Controller;

class AuthController extends BaseController
{
	# 获取Token
	public function token()
	{
		if( $this->checkParams( array( 'appid', 'secret' ) ) )
		{
			$params = $this->_params;

			$Auth = D('Auth');
			$rs = $Auth->checkDeveloper( $params['appid'], $params['secret'] );
	        if( $rs )
	        {
	            $array = explode('.', uniqid($rs, true));

	            $array[0] = $array[0] . dechex(rand(0, 15));
	            $array[1] = substr(md5($array[1]), 8, 16);

	            $token = substr($array[0], 0, 8) . '-' . substr($array[0], 8, 4) . '-' . substr($array[0], 12, 4);
	            $token .= substr($array[1], 0, 4) . '-' . substr($array[1], 4, 12);

	            $expires = time() + 3600;

				session( array( 'id'=>$token, 'name'=>strtolower($token), 'expire'=>3600 ) );
				session( 'expires', $expires );
				session( 'appid', $params['appid'] );

				$mMember = D('Member');
				$member = $mMember->getDetailExt( $params['appid'], $params['secret'] );
				session( 'member', $member );

	            $Auth->setToken( $params['appid'], $token );

	            # 记录API请求日志
	            $parameter = array();
	            $parameter['member_id'] = $member['id'];
	            $parameter['msg'] = '[API请求]';
	            $parameter['type'] = 2; # 1=用户登录 2=API请求
	            $Logs = D('Logs');
	            $Logs->addItem( $parameter );

	            $ret = array( 'token' => $token, 'expires' => $expires );
	            $this->ajaxResponse( 0, 'success', $ret );
	        }
	        else
	            $this->ajaxResponse( 10000, 'appid或secret有误', array() );
		}
	}

}