<?php
namespace Api\Controller;
use Think\Controller;

# 网易云音乐 - 控制器
class NeteaseController extends BaseController
{

	public function _initialize()
	{
		parent::_initialize();
	}

	public function _empty()
	{
		$this->illegalParams();
	}

	public function lrc()
	{
		$params = $this->_params;
		if( $this->checkParams('id') )
		{
			$mNetease = D('Netease');
			$detail = $mNetease->getLrcDetail( intval($params['id']) );

			if( $detail )
				$this->ajaxResponse( 0, 'success', $detail );
			else
				$this->ajaxResponse( 500, 'error' );
		}
	}

}