<?php
namespace Api\Controller;

class AlbumController extends BaseController 
{

    public function _initialize()
    {
        parent::_initialize();        
    }

    public function _empty()
    {
        $this->illegalParams();
    }

    # 获取专辑列表
    public function lists()
    {
        $Album = D('Album');
        $list = $Album->getList();
        
        if( $list )
            $this->ajaxResponse( 0, 'success', $list );
        else
            $this->ajaxResponse( 500, 'error' );
    }

    # 歌曲详情
    public function detail()
    {
        $params = $this->_params;

        $Album = D('Album');
        $detail = $Album->getDetail( $params['id'] );

        if( $detail )
            $this->ajaxResponse( 0, 'success', $detail );
        else
            $this->ajaxResponse( 500, 'error' );
    }

    public function getSession()
    {
        $this->ajaxResponse( 0, 'success', session('member') );
    }

}