<?php
namespace Api\Controller;

class MusicController extends BaseController 
{

    public function _initialize()
    {
        parent::_initialize();
        
        if( !IS_GET )
            $this->ajaxResponse( 500, 'method error' );
    }
    
    public function _empty()
    {
        $this->illegalParams();
    }

    # 获取专辑列表
    public function lists()
    {
        $params = $this->_params;

        $Music = D('Music');
        $list = $Music->getList( $params );
        
        if( $list )
            $this->ajaxResponse( 0, 'success', $list );
        else
            $this->ajaxResponse( 500, 'error' );
    }

    # 歌曲详情
    public function detail()
    {
        $params = $this->_params;

        $Music = D('Music');
        $detail = $Music->getDetail( $params['id'] );

        if( $detail )
            $this->ajaxResponse( 0, 'success', $detail );
        else
            $this->ajaxResponse( 500, 'error' );
    }

    # 修改歌曲
    public function like()
    {
        $params = $this->_params;

        $Music = D('Music');
        $row = $Music->updateLike( $params['id'] );

        if( $row )
            $this->ajaxResponse( 0, 'success', $row );
        else
            $this->ajaxResponse( 500, 'error' );
    }
}