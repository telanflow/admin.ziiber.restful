<?php
namespace Api\Model;
use Think\Model;

class AlbumModel extends BaseModel
{
	protected $mAlbum = null;

	public function _initialize()
	{
		parent::_initialize();

		$this->mAlbum = M('album');
	}

	# 获取专辑列表
	public function getList( $params = array() )
	{
		$result = array();
		$where = array();
		$order = array();
		$order['sort'] = 'asc';

		
		if( session('?member') )
		{
			$member = session('member');
			$where['member_id'] = $member['id'];
		}

		if( isset($params['create_time']) )
			$order['create_time'] = $params['create_time'];

		if( isset($params['status']) )
			$where['status'] = $params['status'];
		else
			$where['status'] = array( 'gt', 0 );

		$list = $this->mAlbum->where($where)->order($order)->select();

		if( $list )
		{
			foreach ($list as $key => $value) 
			{
				unset( $value['member_id'] );
				unset( $value['sort'] );
				unset( $value['create_time'] );
				unset( $value['status'] );

				$result[] = $value;
			}
		}
		else
			$result = false;
		
		return $result;
	}

	# 获取专辑信息
	public function getDetail($id)
	{
		$where = array();
		$where['id'] = $id;

		if( session('?member') )
		{
			$member = session('member');
			$where['member_id'] = $member['id'];
		}

		$detail = $this->mAlbum->where( $where )->find();

		if($detail !== false)
			return $detail;
		else
			return false;
	}
}