<?php
namespace Api\Model;
use Think\Model;

class AuthModel extends BaseModel
{
	protected $tableName = 'member';

	/**
     * 检查开发者身份
     * @param  string $appid  开发者，应用ID
     * @param  string $secret 开发密钥
     * @return mixed          成功返回开发者id，失败返回false
     */
    public function checkDeveloper( $appid , $secret )
    {
        $where = array();
        $where['appid'] = $appid;
        $where['secret'] = $secret;
        $where['status'] = array( 'gt', 0 );
        $where['_logic'] = 'AND';

        $mMember = M('member');
        $detail = $mMember->where( $where )->find();
        
        if( $detail )
            return $detail['appid'];
        else
            return false;
    }

    public function setToken( $appid = null , $token = null )
    {
        if( empty($token) or empty($appid) )
            return false;

        $where = array();
        $where['appid'] = $appid;
        $where['status'] = array( 'gt', 0 );

        $mMember = M('member');
        $row = $mMember->where( $where )->setField( array( 'token'=>$token ) );

        return $row;
    }
}