<?php
namespace Api\Model;
use Think\Model;

class BaseModel extends Model
{
	protected $_session = null;
    protected $_headers = array();

	public function _initialize()
	{
		parent::_initialize();
		$this->_session = session('member');
        $this->_headers = $this->getHeaders();
	}

    # 获取Header头信息
    protected function getHeaders()
    {
        $server = I('server.');
        $header = array();

        foreach ($server as $key => $value)
        {
            if( 'HTTP_' === substr( $key, 0, 5 ) )
            {
                $str = substr( $key, 5 );
                $str = strtolower($str);
                $header[$str] = $value;
            }
        }

        return $header;
    }

    # Header是否存在
    protected function hasHeaders( $key )
    {
        if( !isset($this->_headers[$key]) or empty($this->_headers[$key]) )
            return false;
        else
            return true;
    }

    # 检查缺少的Headers
    protected function checkHeaders( $keys = array() )
    {
        if( is_array($keys) )
        {
            foreach ($keys as $key)
            {
                if( !$this->hasHeaders($key) )
                {
                    $this->lackofHeaders( $key );
                    return false;
                }
            }
        }
        else
        {
            if( !$this->hasHeaders($keys) )
            {
                $this->lackofHeaders( $keys );
                return false;
            }
        }

        return true;
    }

    # 缺少Header的返回
    protected function lackofHeaders( $key )
    {
        # Illegal request
        $this->ajaxResponse( 500, 'invalid ' . $key, array() );
    }
    
}