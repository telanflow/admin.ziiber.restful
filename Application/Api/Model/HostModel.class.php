<?php
namespace Adminis\Model;
use Think\Model;

class HostModel extends BaseModel
{
     /* 模型自动完成 */
    protected $_auto = array(
        array('member_id', 'session', self::MODEL_INSERT, 'function', 'member.id'),
        array('host', 'strtolower', self::MODEL_BOTH, 'function'),
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('status', 1, self::MODEL_INSERT)
    );

    public function _initialize()
    {
        parent::_initialize();
    }

    public function getTotal( $where = array() )
    {
        $number = $this->where( $where )->count();
        return intval($number);
    }

    # 获取专辑列表
    public function getList( $params = array() )
    {
        $where = array();
        $order = array();
 
        # 分页
        if( isset($params['page']) )
            $page = intval($params['page']) < 0 ? 0:intval($params['page']);
        else
            $page = 0;

        # 每页记录数
        if( isset($params['size']) )
            $size = intval($params['size']);
        else
            $size = 20;

        # 排序
        if( isset($params['order']) )
        {
            if( is_array($params['order']) )
            {
                # 格式  'create_time' => 'desc'
                foreach ($params['order'] as $key => $value)
                    $order[$key] = $value;
            }
            else
                $order = $params['order'];
        }
        else
            $order = 'create_time desc';

        $result = array();
        $result['code']                = 0;
        $result['message']             = 'success';
        $result['page']['total']       = $this->getTotal( $where );
        $result['page']['current']     = $page;
        $result['page']['total_pages'] = ceil($result['page']['total'] / $size);
        $result['page']['next']        = $page >= $result['page']['total_pages'] ?  0 : $page + 1;
        $result['page']['before']      = $page <= 1 ?  1 : $page - 1;
        $result['page']['size']        = $size;
        $result['value']               = array();

        if( session('?member') )
        {
            $member = session('member');
            $where['member_id'] = $member['id'];
        }
        else
        {
            $result['code'] = 500;
            $result['message'] = '请登录后操作！';
            return $result;
        }

        $list = $this->where( $where )
                       ->order( $order )
                       ->page( $page, $size )
                       ->select();

        if( $list )
            $result['value'] = $list;
        else
        {
            $result['code'] = 500;
            $result['message'] = 'found Data Error';
        }
        
        return $result;
    }

    # 获取详情
    public function getDetail( $id )
    {
        $detail = $this->field( 'create_time, status', true )->find($id);

        if( $detail )
            return $detail;

        return false;
    }

    # 是否存在域名
    public function hasHost( $host )
    {
        $where = array();

        if( session('?member') )
        {
            $member = session('member');
            $where['member_id'] = intval($member['id']);
            $where['host'] = $host;
            
            $number = $this->where( $where )->count();

            if( intval($number) > 0 )
                return true;
        }

        return false;
    }

    # 获取域名
    public function getHost()
    {
        $where = array();
        $hostStr = array('*');

        if( session('?member') )
        {
            $member = session( 'member' );
            $where['member_id'] = intval($member['id']);
            
            $list = $this->where( $where )->select();

            foreach ($list as $key => $value)
                $hostStr[] = $value['host'];
        }

        $result = implode( ', ', $hostStr);
        return $result;
    }

}