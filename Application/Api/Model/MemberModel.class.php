<?php
namespace Api\Model;
use Think\Model;

class MemberModel extends BaseModel
{
	protected $mMember = null;

	public function _initialize()
	{
		parent::_initialize();

		$this->mMember = M('member');
	}

	# 获取用户列表
	public function getList( $params = array() )
	{
		$where = array();
		$order = array();

		if( isset($params['create_time']) )
			$order['create_time'] = $params['create_time'];
		else
			$order['create_time'] = 'desc';

		if( isset($params['like']) )
			$order['like'] = $params['like'];

		if( isset($params['status']) )
			$where['status'] = $params['status'];
		else
			$where['status'] = array( 'gt', 0 );

		$list = $this->mMember->where($where)->order($order)->select();

		if( $list !== false )
			return $list;
		else
			return false;
	}

	# 获取用户信息
	public function getDetail($id)
	{
		$detail = $this->mMember->find($id);

		if($detail !== false)
			return $detail;
		else
			return false;
	}

	# 获取用户信息
	public function getDetailExt( $appid, $secret )
	{
		$where = array();
		$where['appid'] = $appid;
		$where['secret'] = $secret;
		$where['status'] = array( 'gt', 0 );

		$detail = $this->mMember->where( $where )->find();
		if( $detail )
			return $detail;
		else
			return false;
	}
}