<?php
namespace Api\Model;

class NeteaseModel
{

	# 获取歌曲信息
    public function getDetail( $id )
    {
        $where = array();
        $where['id'] = $id;
        $where['ids'] = '[' . $id . ']';

        $header = array();
        $header['referer'] = 'http://music.163.com/search/';
        $header['cookie'] = 'appver=1.5.0.75771;';

        # http://music.163.com/api/song/detail/?id=354564&ids=[354564]
        $url = 'http://music.163.com/api/song/detail/';

        $row = http( $url, $where, 'get', $header );
        $row = json_decode( $row['content'], true );

        if( $row['songs'] )
        {
            $detail = $row['songs'][0];
            $arr = array();
            $arr['id'] = $detail['id'];
            $arr['audio'] = $detail['mp3Url'];
            $arr['name'] = $detail['name'];
            $arr['album_id'] = $detail['album']['id'];
            $arr['album_name'] = $detail['album']['name'];
            $arr['album_picUrl'] = $detail['album']['picUrl'];
            $arr['artist_id'] = $detail['artists'][0]['id'];
            $arr['artist_name'] = $detail['artists'][0]['name'];
            $arr['artist_picUrl'] = $detail['artists'][0]['picUrl'];
            $arr['lrc'] = U('Netease/lrc@api.ziiber.me', array( 'id'=>$arr['id'] ) );

            return $arr;
        }

        return false;
    }

    # 获取歌词
    public function getLrcDetail( $id )
    {
        $params = array();
        $header = array();
        
        $header['referer'] = 'http://music.163.com/search/';
        $header['cookie'] = 'Cookie: appver=1.5.0.75771;';
        
        $params['os'] = 'pc';
        $params['lv'] = -1;
        $params['kv'] = -1;
        $params['tv'] = -1;
        $params['id'] = $id;

        $url = 'http://music.163.com/api/song/lyric';

        $row = http( $url, $params, 'get', $header );
        $row = json_decode( $row['content'], true );

        if( $row['code'] )
        {
            $arr = array();
            $arr['lrc'] = $row['lrc']["lyric"];
            $arr['krc'] = $row['klyric']["lyric"];
            
            return $arr;
        }

        return false;
    }

}