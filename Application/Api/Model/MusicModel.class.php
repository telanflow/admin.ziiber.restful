<?php
namespace Api\Model;

class MusicModel extends BaseModel
{
	protected $mMusic = null;

	public function _initialize()
	{
		parent::_initialize();

		$this->mMusic = M('music');
	}

	# 获取歌曲列表
	public function getList( $params = array() )
	{
		$result = array();
		$where = array();
		$order = array();

		if( session('?member') )
		{
			$member = session('member');
			$where['member_id'] = $member['id'];
		}

		if( isset($params['create_time']) )
			$order['create_time'] = $params['create_time'];
		else
			$order['create_time'] = 'desc';

		if( isset($params['like']) )
			$order['like'] = $params['like'];

		if( isset($params['album_id']) )
			$where['album_id'] = intval($params['album_id']);

		if( isset($params['status']) )
			$where['status'] = $params['status'];
		else
			$where['status'] = array( 'gt', 0 );

		$list = $this->mMusic->where($where)->order($order)->select();

		if( $list )
		{
			foreach ($list as $key => $value)
			{
				unset( $value['member_id'] );
				unset( $value['album_id'] );
				unset( $value['like'] );
				unset( $value['create_time'] );
				unset( $value['status'] );

				$result[] = $value;
			}

		}
		else
			$result = false;

		return $result;
	}

	# 获取歌曲信息
	public function getDetail($id)
	{
		$where = array();
		$where['id'] = $id;

		if( session('?member') )
		{
			$member = session('member');
			$where['member_id'] = $member['id'];
		}

		$detail = $this->mMusic->where( $where )->find();

		if($detail !== false)
			return $detail;
		else
			return false;
	}

	public function updateLike($id)
	{
		$row = $this->mMusic->where( array('id'=>$id) )->setInc('like');

		if($row !== false)
			return $row;
		else
			return false;
	}
}