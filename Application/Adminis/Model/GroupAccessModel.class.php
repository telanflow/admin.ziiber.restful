<?php
namespace Adminis\Model;

# 用户权限 明细表
class GroupAccessModel extends BaseModel
{
	protected $tableName = 'auth_group_access';

	# 设置用户的用户组
	public function setItem( $uid, $group_id )
	{
		$params = array( 'group_id'=>$group_id );
		$result = $this->where( array('uid'=>$uid) )->save( $params );

		if( $result )
			return true;
		else
			return false;
	}

	# 获取用户组ID
	public function getGroup( $uid )
	{
		$group_id = $this->where( array('uid'=>$uid) )->getField('group_id');
		return $group_id;
	}

    # 查询分组规则信息
    public function allRule()
    {
    	$mRule = M('AuthRule'); 
    	$res = $mRule->where('pid = 0')->select();
    	$list = array();
    	foreach($res as $k => $v)
    	{
    		$list[$k]['title'] = $v;
    		$rs = $mRule->where('pid = ' . $v['id'])->select();
    		$list[$k]['rules'] = $rs;
    	}

    	return $list;
    }

    # 查询当前要编辑的用户
    public function userrow( $id )
    {
        $rule = M('authGroup')->find( $id );
        return $rule;
    }

    # 规则设置
    public function setRule( $data )
    {
        $ruleId = $menuId = array();
        foreach($data['data'] as $k => $v)
        {
            $arr = explode(':', $v);
            array_push($ruleId, $arr[0]);
            array_push($menuId, $arr[1]);
        }

        $rid = implode(',', $ruleId);
        $mid = implode(',', array_unique($menuId));
        $rule['rules'] = $mid . ',' . $rid;

        $res = M('authGroup')->where( array('id'=>$data['id']) )->save( $rule );
        
        return $res;
    }

}