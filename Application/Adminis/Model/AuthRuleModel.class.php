<?php
namespace Adminis\Model;
use Think\Model;

# 用户规则表
class AuthRuleModel extends BaseModel
{
    public function _initialize()
    {
        parent::_initialize();
    }

    # 获取记录数
    public function getTotal( $where = array() )
    {
        $mRule = M('authRule');
        $number = $mRule->where( $where )->count();
        return intval($number);
    }

    # 获取规则列表
    public function getList( $params = array() )
    {
        $mRule = M('authRule');
        $where = array();
        $order = array();

        if( session('?member') )
        {
            $member = session('member');
            $where['member_id'] = $member['id'];
        }

        if( isset($params['pid']) )
            $where['pid'] = intval($params['pid']);

        # 分页
        if( isset($params['page']) )
            $page = intval($params['page']) < 0 ? 0:intval($params['page']);
        else
            $page = 0;

        # 每页记录数
        if( isset($params['size']) )
            $size = intval($params['size']);
        else
            $size = 20;

        # 排序
        if( isset($params['order']) )
        {
            if( is_array($params['order']) )
            {
                # 格式  'create_time' => 'desc'
                foreach ($params['order'] as $key => $value)
                    $order[$key] = $value;
            }
            else
                $order = $params['order'];
        }

        $result = array();
        $result['code']                = 0;
        $result['message']             = 'success';
        $result['page']['total']       = $this->getTotal( $where );
        $result['page']['current']     = $page;
        $result['page']['total_pages'] = ceil($result['page']['total'] / $size);
        $result['page']['next']        = $page >= $result['page']['total_pages'] ?  0 : $page + 1;
        $result['page']['before']      = $page <= 1 ?  1 : $page - 1;
        $result['page']['size']        = $size;
        $result['value']               = array();

        $list = $mRule->where( $where )->order( $order )->page( $page, $size )->select();

        if( $list )
            $result['value'] = $list;
        else
        {
            $result['code'] = 500;
            $result['message'] = 'found Data Error';
        }
        
        return $result;
    }

    # 详情
    public function getDetail( $id )
    {
        $mRule = M('authRule');
        $detail = $mRule->find( $id );

        if($detail)
            return $detail;
        else
            return false;
    }

    # 添加规则
    public function addItem( $params = array() )
    {
        $mRule = M('authRule');

        $row = $mRule->create( $params );
        if( $row !== false )
        {
            $id = $mRule->add();
            if( $id )
                return $id;
        }

        return false;
    }

    # 编辑规则
    public function editItem( $id, $params = array() )
    {
        $mRule = M('authRule');

        $row = $mRule->where( array('id'=>$id) )->save( $params );

        if( $row !== false )
            return true;
        else
            return false;
    }

    # 修改状态
    public function updataStatus( $id, $status = 1 )
    {
        $mRule = M('authRule');
        $params = array();
        $params['status'] = $status;
        $result = $mRule->where( array('id'=>$id) )->setField( $params );

        if( $result !== false )
            return $result;
        else
            return false;
    }
}
