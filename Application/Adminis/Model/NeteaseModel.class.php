<?php
namespace Adminis\Model;

class NeteaseModel
{

    # 搜索歌曲
    public function search( $params = array() )
    {
        $where = array();
        $header = array();
        $header['referer'] = 'http://music.163.com/search/';
        $header['cookie'] = 'Cookie: appver=1.5.0.75771;';

        # 搜索内容
        if( isset($params['title']) )
            $where['s'] = $params['title'];

        # 搜索类型  歌曲 1  专辑 10 歌手 100 歌单 1000 用户 1002 mv 1004 歌词 1006   主播电台 1009
        if( isset($params['type']) )
            $where['type'] = intval($params['type']);
        else
            $where['type'] = 1;

        # 页码
        if( isset($params['page']) )
        {
            $page = intval($params['page']) - 1;
            $page = $page >= 0 ? $page:0;
        }
        else
            $page = 0;

        # 记录数
        if( isset($params['size']) )
            $size = intval($params['size']);
        else
            $size = 20;

        $where['limit'] = $size;
        $where['offset'] = $page * $size;

        #http://s.music.163.com/search/get/?limit=10&s=海阔天空&type=1&offset=1
        $url = 'http://s.music.163.com/search/get/';

        $row = http( $url, $where, 'get', $header );
        $row = json_decode( $row['content'], true );

        $result = array();
        $result['code']                = 0;
        $result['message']             = 'success';
        $result['page']['total']       = intval($row['result']['songCount']);
        $result['page']['current']     = $page;
        $result['page']['total_pages'] = ceil($result['page']['total'] / $size);
        $result['page']['next']        = $page >= $result['page']['total_pages'] ?  1 : $page + 1;
        $result['page']['before']      = $page <= 1 ?  1 : $page - 1;
        $result['page']['size']        = $size;
        $result['value']               = array();

        if( $row['result'] )
        {
            foreach ($row['result']['songs'] as $key => $value)
            {
                $arr = array();
                $arr['id'] = $value['id'];
                $arr['name'] = $value['name'];
                $arr['artists_id'] = $value['artists'][0]['id'];
                $arr['artists_name'] = $value['artists'][0]['name'];
                $arr['artists_picUrl'] = $value['artists'][0]['picUrl'];
                $arr['album_id'] = $value['album']['id'];
                $arr['album_name'] = $value['album']['name'];
                $arr['album_picUrl'] = $value['album']['picUrl'];
                $arr['music'] = $value['audio'];

                $result['value'][] = $arr;
            }
        }
        else
        {
            $result['code'] = 500;
            $result['message'] = 'not found data';
        }

        return $result;
    }

    # 获取歌曲信息
    public function getDetail( $id )
    {
        $where = array();
        $where['id'] = $id;
        $where['ids'] = '[' . $id . ']';

        $header = array();
        $header['referer'] = 'http://music.163.com/search/';
        $header['cookie'] = 'Cookie: appver=1.5.0.75771;';

        # http://music.163.com/api/song/detail/?id=354564&ids=[354564]
        $url = 'http://music.163.com/api/song/detail/';

        $row = http( $url, $where, 'get', $header );
        $row = json_decode( $row['content'], true );

        if( $row['songs'] )
        {
            $detail = $row['songs'][0];
            $arr = array();
            $arr['id'] = $detail['id'];
            $arr['audio'] = $detail['mp3Url'];
            $arr['name'] = $detail['name'];
            $arr['album_id'] = $detail['album']['id'];
            $arr['album_name'] = $detail['album']['name'];
            $arr['album_picUrl'] = $detail['album']['picUrl'];
            $arr['artist_id'] = $detail['artists'][0]['id'];
            $arr['artist_name'] = $detail['artists'][0]['name'];
            $arr['artist_picUrl'] = $detail['artists'][0]['picUrl'];
            $arr['lrc'] = U('Netease/lrc@api.ziiber.me', array( 'id'=>$arr['id'] ) );

            return $arr;
        }

        return false;
    }

    # 获取歌词
    public function getLrcDetail( $id )
    {
        $params = array();
        $header = array();
        
        $header['referer'] = 'http://music.163.com/search/';
        $header['cookie'] = 'Cookie: appver=1.5.0.75771;';
        
        $params['os'] = 'pc';
        $params['lv'] = -1;
        $params['kv'] = -1;
        $params['tv'] = -1;
        $params['id'] = $id;

        $url = 'http://music.163.com/api/song/lyric';

        $row = http( $url, $params, 'get', $header );
        $row = json_decode( $row['content'], true );

        if( $row['code'] )
        {
            $arr = array();
            $arr['lrc'] = $row['lrc'];
            $arr['krc'] = $row['klyric'];

            return $arr;
        }

        return false;
    }

    # 歌手专辑
    public function getAlbumInfo( $id )
    {
        $params = array();
        $header = array();

        $header['referer'] = 'http://music.163.com/search/';
        $header['cookie'] = 'Cookie: appver=1.5.0.75771;';

        $url = 'http://music.163.com/api/artist/albums/' . $id . '?id=' . $id . '&offset=0&to';
        $row = http( $url, null, 'get', $header );
        $row = json_decode( $row['content'], true );

        if( $row['code'] == 200 )
        {
            
            return $row;
        }

        return false;
    }

}