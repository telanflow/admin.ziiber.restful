<?php
namespace Adminis\Model;

# 用户组表
class AuthGroupModel extends BaseModel
{
    public function _initialize()
    {
        parent::_initialize();
    }

    # 获取记录数
    public function getTotal( $where = array() )
    {
        $mGroup = M('authGroup');
        $number = $mGroup->where( $where )->count();
        return intval($number);
    }

    # 获取用户组列表
    public function getList( $params = array() )
    {
        $mGroup = M('authGroup');
        $where = array();
        $order = array();

        if( session('?member') )
        {
            $member = session('member');
            $where['member_id'] = $member['id'];
        }

        # 分页
        if( isset($params['page']) )
            $page = intval($params['page']) < 0 ? 0:intval($params['page']);
        else
            $page = 0;

        # 每页记录数
        if( isset($params['size']) )
            $size = intval($params['size']);
        else
            $size = 20;

        $result = array();
        $result['code']                = 0;
        $result['message']             = 'success';
        $result['page']['total']       = $this->getTotal( $where );
        $result['page']['current']     = $page;
        $result['page']['total_pages'] = ceil($result['page']['total'] / $size);
        $result['page']['next']        = $page >= $result['page']['total_pages'] ?  0 : $page + 1;
        $result['page']['before']      = $page <= 1 ?  1 : $page - 1;
        $result['page']['size']        = $size;
        $result['value']               = array();

        $list = $mGroup->where( $where )->order( $order )->page( $page, $size )->select();

        if( $list )
            $result['value'] = $list;
        else
        {
            $result['code'] = 500;
            $result['message'] = 'found Data Error';
        }
        
        return $result;
    }

    # 添加规则
    public function addItem( $params = array() )
    {
        $mGroup = M('authGroup');

        foreach ($params as $key => $value)
        {
            $mGroup->$key = $value;
        }

        $id = $mGroup->add();
        if( $id )
            return $id;
        else
            return false;
    }

    # 编辑规则
    public function editItem( $id, $params = array() )
    {
        $mGroup = M('authGroup');

        $row = $mGroup->where( array('id'=>$id) )->save( $params );

        if( $row !== false )
            return true;
        else
            return false;
    }

}