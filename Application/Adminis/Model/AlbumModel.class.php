<?php
namespace Adminis\Model;
use Think\Model;

class AlbumModel extends BaseModel
{
	public function _initialize()
	{
		parent::_initialize();
	}

	public function getTotal( $where = array() )
	{
		$mMuzikland = M('album');
		$number = $mMuzikland->where( $where )->count();
		return intval($number);
	}

	# 获取专辑列表
	public function getList( $params = array() )
	{
		$mMuzikland = M('album');
		$where = array();
		$order = array();

		if( session('?member') )
		{
			$member = session('member');
			$where['member_id'] = $member['id'];
		}
 
		# 分页
		if( isset($params['page']) )
			$page = intval($params['page']) < 0 ? 0:intval($params['page']);
		else
			$page = 0;

		# 每页记录数
		if( isset($params['size']) )
			$size = intval($params['size']);
		else
			$size = 20;

		# 排序
		if( isset($params['order']) )
		{
			if( is_array($params['order']) )
			{
				# 格式  'create_time' => 'desc'
				foreach ($params['order'] as $key => $value)
					$order[$key] = $value;
			}
			else
				$order = $params['order'];
		}
		else
			$order = 'id desc,create_time desc';

		$result = array();
        $result['code']                = 0;
        $result['message']             = 'success';
        $result['page']['total']       = $this->getTotal( $where );
        $result['page']['current']     = $page;
        $result['page']['total_pages'] = ceil($result['page']['total'] / $size);
        $result['page']['next']        = $page >= $result['page']['total_pages'] ?  0 : $page + 1;
        $result['page']['before']      = $page <= 1 ?  1 : $page - 1;
        $result['page']['size']        = $size;
        $result['value']               = array();

		$list = $mMuzikland->where( $where )
						   ->order( $order )
						   ->page( $page, $size )
						   ->select();

		if( $list )
			$result['value'] = $list;
		else
		{
			$result['code'] = 500;
			$result['message'] = 'found Data Error';
		}
		
		return $result;
	}

	# 获取详情
	public function getDetail( $id )
	{
		$mAlbum = M('album');
		$detail = $mAlbum->field( 'create_time, status', true )->find($id);

		if( $detail )
			return $detail;

		return false;
	}

	# 编辑
	public function editItem( $id, $params = array() )
	{
		$where = array();
		$where['id'] = intval($id);

		if( session('?member') )
		{
			$member = session('member');
			$where['member_id'] = $member['id'];
		}

		$mAlbum = M( 'album' );
		if( $mAlbum->create( $params ) !== false )
		{

			$row = $mAlbum->where( $where )->save();

			if( $row !== false )
				return true;
		}
		
		return false;
	}

	# 删除
	public function removeItem( $id )
	{
		$mAlbum = M('album');
		$mMusic = M('music');

		$where = array();
		$where['id'] = $id;

		# 获取当前登录的用户ID
		if( session('?member') )
		{
			$member = session('member');
			$were['member_id'] = $member['id'];
		}

		# 删除专辑下的歌曲
		$result = $mMusic->where( array( 'album_id'=>$id, 'member_id'=>$member['id'] ) )->delete();

		# 删除专辑
		$row = $mAlbum->where( $where )->delete();
		if( $row )
			return true;

		return false;
	}

	# 添加
	public function addItem( $params = array() )
	{
		if( session('?member') )
		{
			$member = session('member');
			$params['member_id'] = $member['id'];
		}

		$params['create_time'] = time();
		$params['status'] = 1;

		$mAlbum = M( 'album' );

		# 是否存在专辑
		$where = array();
		$where['member_id'] = $member['id'];
		$list = $mAlbum->where( $where )->select();
		if( $list )
			$params['defaults'] = 0;
		else
			$params['defaults'] = 1;

		# 准备数据
		if( $mAlbum->create( $params ) !== false )
		{
			$id = $mAlbum->add();

			if( $id !== false )
				return $id;
		}

		return false;
	}

	# 排序
	public function editSort( $params = array() )
	{
		$mAlbum = M('album');

		foreach ($params as $item) 
		{
			$mAlbum->where( 'id='.$item['id'] )->setField( 'sort', intval($item['sort']) );
		}
		
		return true;
	}

	# 设置默认专辑
	public function setDefault( $id )
	{
		$where = array();

		if( session('?member') )
		{
			$member = session('member');
			$where['member_id'] = $member['id'];
		}

		$mAlbum = M('album');
		$mAlbum->where( $where )->setField( 'defaults', 0 );

		$where['id'] = $id;
		$row = $mAlbum->where( $where )->setField( 'defaults', 1 );

		if( $row )
			return $row;
		else
			return false;
	}
}