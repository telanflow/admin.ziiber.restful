<?php
namespace Adminis\Model;
use Think\Model;

class MusicModel extends BaseModel
{
	public function _initialize()
	{
		parent::_initialize();
	}

	# 查询数量
	public function getTotal( $where = array() )
	{
		$mMusic = M('music');
		$number = $mMusic->where( $where )->count();
		return intval($number);
	}

	# 获取歌单列表
	public function getList( $params = array() )
	{
		$mMusic = M('music');
		$where = array();
		$order = array();

		if( session('?member') )
		{
			$member = session('member');
			$where['member_id'] = $member['id'];
		}

		# 专辑分类
		if( isset($params['album_id']) )
			$where['album_id'] = intval($params['album_id']);

		# 分页
		if( isset($params['page']) )
			$page = intval($params['page']) < 0 ? 0:intval($params['page']);
		else
			$page = 0;

		# 每页记录数
		if( isset($params['size']) )
			$size = intval($params['size']);
		else
			$size = 20;

		# 排序
		if( isset($params['order']) )
		{
			if( is_array($params['order']) )
			{
				# 格式  'create_time' => 'desc'
				foreach ($params['order'] as $key => $value)
					$order[$key] = $value;
			}
			else
				$order = $params['order'];
		}
		else
			$order = 'create_time desc';

		$result = array();
        $result['code']                = 0;
        $result['message']             = 'success';
        $result['page']['total']       = $this->getTotal( $where );
        $result['page']['current']     = $page;
        $result['page']['total_pages'] = ceil($result['page']['total'] / $size);
        $result['page']['next']        = $page >= $result['page']['total_pages'] ?  0 : $page + 1;
        $result['page']['before']      = $page <= 1 ?  1 : $page - 1;
        $result['page']['size']        = $size;
        $result['value']               = array();

		$list = $mMusic->alias('r')
					   ->where( $where )
					   ->order( $order )
					   ->page( $page, $size )
					   ->select();

		if( $list )
			$result['value'] = $list;
		else
		{
			$result['code'] = 500;
			$result['message'] = 'found Data Error';
		}
		
		return $result;
	}

	# 编辑
	public function editItem( $id, $params = array() )
	{
		$mMusic = M('music');

		foreach ($params as $key => $value)
			$mMusic->$key = $value;

		$field = array(
			'id',
			'member_id',
			'create_time',
			'status'
		);

		$row = $mMusic->field( $field, true )->where( array( 'id'=>$id ) )->save();
		if( $row !== false )
			return true;
		else
			return false;
	}

	# 删除音乐
	public function removeItem( $id )
	{
		$mMusic = M( 'music' );
		$row = $mMusic->where( array( 'id' => $id ) )->delete();
		
		if( $row !== false )
			return true;
		else
			return false;
	}

	# 获取信息
	public function getDetail( $id )
	{
		$mMusic = M( 'music' );
		$detail = $mMusic->field( 'create_time, status, member_id', true )->find( $id );

		if( $detail )
			return $detail;
		else
			return false;
	}

	# 添加
	public function addItem( $params = array() )
	{
		if( session('?member') )
		{
			$member = session('member');
			$params['member_id'] = $member['id'];
		}
		else
			$params['member_id'] = 1;

		$params['create_time'] = time();
		$params['like'] = 0;
		$params['status'] = 1;

		$mMusic = M( 'music' );
		if( $mMusic->create( $params ) !== false )
		{
			$id = $mMusic->add();

			if( $id !== false )
				return $id;
		}

		return false;
	}

}