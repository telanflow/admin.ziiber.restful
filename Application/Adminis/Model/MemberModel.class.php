<?php
namespace Adminis\Model;

class MemberModel extends BaseModel
{
    public function _initialize()
    {
        parent::_initialize();
    }

	# 登录
	public function login( $email, $password )
	{
		# 1  密码正确，登录成功
		# -1 不存在用户
		# -2 密码不正确

		$where = array();
		$result = 0;

		$where['email'] = $email;
		$where['status'] = array( 'gt', 0 );

		$mMember = M('member');
		$pwd = $mMember->where( $where )->getField( 'password' );

		if( $pwd )
		{
			# 存在用户名
			if( $pwd === $password )
			{
				# 密码正确，登录成功
				$mMember->login_time = time();
				$mMember->login_ip = ip2long(get_client_ip());
				$mMember->where( $where )->save();

				$member = $mMember->where( $where )->find();
				$member['login_ip'] = long2ip($member['login_ip']);
                $member['group_id'] = D('GroupAccess')->getGroup( $member['id'] );
				
                session( 'member', $member );

                # 记录日志
                $params = array();
                $params['member_id'] = $member['id'];
                $params['msg'] = '[用户登录]';
                $params['type'] = 1; # 1=用户登录 2=API请求
                
                $Logs = D('Logs');
                $id = $Logs->addItem( $params );

				$result = 1;
			}
			else
			{
				# 密码不正确
				$result = -2;
			}
		}
		else
		{
			# 不存在用户
			$result = -1;
		}

		return $result;
	}

	# 退出
	public function logout()
	{
		session('member', null);
        // session('user_auth_sign', null);
        session('[destroy]');
	}

	/**
     * 注册一个新用户
     * @param  string $email    用户邮箱
     * @param  string $password 用户密码
     * @return integer          注册成功-用户信息，注册失败-错误编号
     */
    public function register( $email, $password )
    {
        $data = $this->create();

        var_dump($data);exit;

        /* 添加用户 */
        if ($this->create($data))
        {
            $uid = $this->add();
            return $uid ? $uid : 0; //0-未知错误，大于0-注册成功
        }
        else
        {
            return $this->getError(); //错误详情见自动验证注释
        }
    }

    # 生成新的appid   11
    public function appid()
    {
        $array = explode('.', uniqid($rs, true));

        $array[0] = $array[0] . dechex(rand(0, 15));
        $array[1] = substr(md5($array[1]), 8, 16);
        $i = $array[0] . $array[1];
        $i = substr( $i , rand(1,20), 8 );
        $s = substr( $i , 2, 8 );
        $char = base64_encode(dechex(rand(1, 99999)));

        $keyStr = substr($char,1,2) . strtoupper($i) . strtolower($s);
        $appid = substr( $keyStr, rand(0,5), 11 );
        return $appid;
    }

    # 生成新的secret  16
    public function secret()
    {
        $array = explode('.', uniqid($rs, true));

        $array[0] = $array[0] . dechex(rand(0, 15));
        $array[1] = substr(md5($array[1]), 8, 16);
        $i = $array[0] . $array[1];
        $i = substr( $i , rand(1,20), 8 );
        $s = substr( $i , 2, 8 );
        $char = base64_encode(dechex(rand(1, 99999)));
        
        $keyStr = substr($char,1,2) . strtoupper($i) . strtolower($s);
        return $keyStr;
    }

    # 获取记录数
    public function getTotal( $where = array() )
    {
        $mMember = M('member');
        $number = $mMember->where()->count();
        return intval($number);
    }

    # 获取用户列表
    public function getList( $params = array() )
    {
        $mMember = M('member');
        $where = array();
        $order = array();
        
        # 排除系统管理员
        $where['id'] = array( 'gt', 0 );

        # 分页
        if( isset($params['page']) )
            $page = intval($params['page']) < 0 ? 0:intval($params['page']);
        else
            $page = 0;

        # 每页记录数
        if( isset($params['size']) )
            $size = intval($params['size']);
        else
            $size = 20;

        # 排序
        if( isset($params['order']) )
        {
            if( is_array($params['order']) )
            {
                # 格式  'create_time' => 'desc'
                foreach ($params['order'] as $key => $value)
                    $order[$key] = $value;
            }
            else
                $order = $params['order'];
        }
        else
            $order = 'create_time desc';

        $result = array();
        $result['code']                = 0;
        $result['message']             = 'success';
        $result['page']['total']       = $this->getTotal( $where );
        $result['page']['current']     = $page;
        $result['page']['total_pages'] = ceil($result['page']['total'] / $size);
        $result['page']['next']        = $page >= $result['page']['total_pages'] ?  0 : $page + 1;
        $result['page']['before']      = $page <= 1 ?  1 : $page - 1;
        $result['page']['size']        = $size;
        $result['value']               = array();

        $list = $mMember->where( $where )->order( $order )->page( $page, $size )->select();

        if( $list )
            $result['value'] = $list;
        else
        {
            $result['code'] = 500;
            $result['message'] = 'not Found User';
        }

        return $result;
    }

    # 修改用户状态
    public function editItem( $id, $params = array() )
    {
        $mMember = M('member');

        $row = $mMember->create( $params );
        if( $row !== false )
        {
            $row = $mMember->where( array( 'id'=>$id ) )->save();
            return $row;
        }

        return false;
    }

    # 更新用户状态
    public function updataStatus( $id, $status = 0 )
    {
        $mMember = M('member');
        $mMember->status = $status;
        $result = $mMember->where( array('id'=>$id) )->save();

        if( $result )
            return true;
        else
            return false;
    }

    
    
}