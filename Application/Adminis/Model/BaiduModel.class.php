<?php
namespace Adminis\Model;

class BaiduModel
{

	# 搜索歌曲
	public function search( $params = array() )
	{
		$where = array();

		$where[] = 'from=qianqian';
		$where[] = 'version=2.1.0';
		$where[] = 'method=baidu.ting.search.common';
		$where[] = 'format=json';

		# 搜索内容
		if( isset($params['title']) )
			$where[] = 'query=' . $params['title'];

		# 页码
		if( isset($params['page']) )
			$page = intval($params['page']);
		else
			$page = 1;

		# 记录数
		if( isset($params['size']) )
			$size = intval($params['size']);
		else
			$size = 20;


		$where[] = 'page_no=' . $page;
		$where[] = 'page_size=' . $size;

		$url = 'http://tingapi.ting.baidu.com/v1/restserver/ting?' . implode( '&' , $where );

		$row = http( $url );
		$row = json_decode( $row['content'], true );

		$result = array();
        $result['code']                = 0;
        $result['message']             = 'success';
        $result['page']['total']       = intval($row['pages']['total']);
        $result['page']['current']     = $page;
        $result['page']['total_pages'] = ceil($result['page']['total'] / $size);
        $result['page']['next']        = $page >= $result['page']['total_pages'] ?  1 : $page + 1;
        $result['page']['before']      = $page <= 1 ?  1 : $page - 1;
        $result['page']['size']        = $size;
        $result['value']               = array();

        if( $row['song_list'] )
        {
        	$result['value'] = $row['song_list'];
        }
        else
        {
        	$result['code'] = 500;
        	$result['message'] = 'not found data';
        }

		return $result;
	}

	# 获取歌曲信息
	public function getDetail( $id )
	{
		$url = 'http://ting.baidu.com/data/music/links?songIds=' . $id;

		$row = http( $url );
		$row = json_decode( $row['content'], true );

		if( $row['errorCode'] == 22000 )
		{
			$detail = $row['data']['songList'];

			foreach ($detail as $key => $value) {
				$detail[$key]['lrcLink'] = 'http://ting.baidu.com' . $value['lrcLink'];
			}

			return $detail;
		}

		return false;
	}

}