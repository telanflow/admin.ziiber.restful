<?php
namespace Adminis\Model;

class LogsModel extends BaseModel
{
    protected $_mLogs = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->_mLogs = M( 'logs' );
    }

    # 查询数量
    public function getTotal( $where = array() )
    {
        $number = $this->_mLogs->where( $where )->count();
        return intval($number);
    }

    # 添加
    public function addItem( $params = array() )
    {
        if( $this->_mLogs->create( $params ) !== false )
        {
            if( empty($params['member_id']) && session('?member') )
            {
                $member = session( 'member' );
                $this->_mLogs->member_id = $member['id'];
            }

            $this->_mLogs->user_agent = $this->_headers['user_agent'];
            $this->_mLogs->create_time = time();
            $this->_mLogs->ip = ip2long( get_client_ip() );

            $row = $this->_mLogs->add();

            if( $row ) return $row;
        }

        return false;
    }

    # 获取列表
    public function getList( $params = array() )
    {
        $where = array();
        $order = array();

        if( session('?member') )
        {
            $member = session('member');
            if( intval($member['id']) > 0 )
                $where['member_id'] = $member['id'];
        }

        # 分类  1 = 用户登录  2 = API请求
        if( isset($params['type']) )
            $where['type'] = intval($params['type']);

        # 分页
        if( isset($params['page']) )
            $page = intval($params['page']) < 1 ? 1:intval($params['page']);
        else
            $page = 1;

        # 每页记录数
        if( isset($params['size']) )
            $size = intval($params['size']);
        else
            $size = 20;

        # 排序
        if( isset($params['order']) )
        {
            if( is_array($params['order']) )
            {
                # 格式  'create_time' => 'desc'
                foreach ($params['order'] as $key => $value)
                    $order[$key] = $value;
            }
            else
                $order = $params['order'];
        }
        else
            $order = 'create_time desc';
        
        $result = array();
        $result['code']                = 0;
        $result['message']             = 'success';
        $result['page']['total']       = $this->getTotal( $where );
        $result['page']['current']     = $page;
        $result['page']['total_pages'] = ceil($result['page']['total'] / $size);
        $result['page']['next']        = $page >= $result['page']['total_pages'] ?  0 : $page + 1;
        $result['page']['before']      = $page <= 1 ?  1 : $page - 1;
        $result['page']['size']        = $size;
        $result['value']               = array();

        $list = $this->_mLogs
                       ->where( $where )
                       ->order( $order )
                       ->page( $page, $size )
                       ->select();

        if( $list )
            $result['value'] = $list;
        else
        {
            $result['code'] = 500;
            $result['message'] = 'found Data Error';
        }
        
        return $result;
    }

}