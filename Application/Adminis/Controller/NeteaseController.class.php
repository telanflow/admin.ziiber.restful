<?php
namespace Adminis\Controller;
use Think\Page;

class NeteaseController extends BaseController
{
    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
    	$params = $this->_params;
		
		$mNetease = D( 'Netease' );
		$list = $mNetease->search( $params );

		$mAlbum= D('Album');
		$albumList = $mAlbum->getList();

    	# 实例化分页类 传入总记录数和每页显示的记录数
		$Page       = new Page( $list['page']['total'], $list['page']['size'] );
		$Page->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
		# 分页显示输出
		$show       = $Page->show();
		$this->pageContent = $show;
		$this->page = $list['page'];

		$this->albumList = $albumList['value'];
		$this->lists = $list['value'];
        $this->display();
    }

    # 获取音乐详情
    public function detail()
    {
    	if( IS_POST )
		{
			if( $this->hasParams('id') )
			{
				$params = $this->_params;

				$mNetease = D('Netease');
				$detail = $mNetease->getDetail( $params['id'] );
				
				if( $detail )
					$this->ajaxResponse( 0, 'success', $detail );
				else
					$this->ajaxResponse( 500, '暂无歌曲信息！' );
			}
			else
				$this->ajaxResponse( 500, '缺少参数：id' );
		}
    }

    public function key()
    {
    	$mNetease = D( 'Netease' );
    	$row = $mNetease->getLrcDetail(  );

    	var_dump($row);

    }

}