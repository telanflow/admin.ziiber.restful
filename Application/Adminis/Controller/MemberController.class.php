<?php
namespace Adminis\Controller;
use Think\Controller;

class MemberController extends Controller
{

	public function _initialize()
	{
		if( session('?member') && ACTION_NAME != 'logout' )
		{
			$this->redirect( 'Index/index' );
		}
	}

	# 登陆页面
	public function login()
	{
		if( IS_POST )
		{
			$params = I('post.');
			$ret = array(
				'code' => 500,
				'message' => 'success',
				'value' => ''
			);

			$mMember = D('member');
			$result = $mMember->login( $params['email'], $params['password'] );

			if( $result === 1 )
			{
				# 登录成功
				$ret['code'] = 0;
				$ret['message'] = '登陆成功！';
				$ret['value'] = U('Index/index');
			}
			elseif( $result === -1 )
			{
				# 用户不存在
				$ret['code'] = 500;
				$ret['message'] = '登录失败，用户名或密码错误！';
			}
			elseif( $result === -2 )
			{
				# 密码错误
				$ret['code'] = 500;
				$ret['message'] = '登录失败，密码错误！';
			}
			
			$this->ajaxReturn( $ret );
		}
		else
		{

			$this->display();
		}
	}

	public function logout()
	{
		$mMember = D('Member');
		$mMember->logout();
		$this->redirect( 'Member/login' );
	}
}