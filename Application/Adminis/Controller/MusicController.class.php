<?php
namespace Adminis\Controller;
use Think\Controller;
use Think\Page;

class MusicController extends BaseController
{

	public function _initialize()
	{
		parent::_initialize();
	}

	public function index()
	{
		$params = $this->_params;
		
		$mMusic = D( 'Music' );
		$list = $mMusic->getList( $params );

		$mAlbum = D( 'Album' );
		$albumList = $mAlbum->getList( $params );

		# 实例化分页类 传入总记录数和每页显示的记录数
		$Page       = new Page( $list['page']['total'], $list['page']['size'] );
		$Page->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
		# 分页显示输出
		$show       = $Page->show();
		$this->pageContent = $show;
		$this->page = $list['page'];

		$this->albumList = $albumList['value'];
		$this->lists = $list['value'];
		$this->display();
	}

	# 删除音乐
	public function removeMusic()
	{
		if( IS_POST )
		{
			$params = $this->_params;
			if( $this->hasParams('id') )
			{
				$mMusic = D( 'Music' );
				$row = $mMusic->removeItem( $params['id'] );
				if( $row )
					$this->ajaxResponse( 0, '删除成功！' );
				else
					$this->ajaxResponse( 500, '删除失败，系统错误！' );
			}
			else
				$this->ajaxResponse( 500, '缺少参数：id' );
		}
	}

	# 获取音乐信息
	public function detail()
	{
		if( IS_POST )
		{
			$params = $this->_params;
			if( $this->hasParams('id') )
			{
				$mMusic = D( 'Music' );
				$detail = $mMusic->getDetail( $params['id'] );

				if( $detail )
					$this->ajaxResponse( 0, 'success', $detail );
				else
					$this->ajaxResponse( 500, 'error' );
			}
			else
				$this->ajaxResponse( 500, '缺少参数：id' );
		}
	}
	
	# 编辑音乐信息
	public function editDetail()
	{
		if( IS_POST )
		{
			$params = $this->_params;
			if( $this->hasParams('id') )
			{
				# 编辑
				$id = intval($params['id']);
				unset($params['id']);

				$mMusic = D( 'Music' );
				$row = $mMusic->editItem( $id, $params );

				if( $row )
					$this->ajaxResponse( 0, '保存成功！', $row );
				else
					$this->ajaxResponse( 500, 'error' );
			}
			else
			{
				# 新增
				$mMusic = D( 'Music' );
				$row = $mMusic->addItem( $params );

				if( $row )
					$this->ajaxResponse( 0, '音乐添加成功！', $row );
				else
					$this->ajaxResponse( 500, 'error' );
			}
		}
	}

}