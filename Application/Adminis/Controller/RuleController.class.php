<?php
namespace Adminis\Controller;
use Think\Page;

class RuleController extends BaseController
{

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        $params = $this->_params;

        if( IS_POST )
        {
            if( $this->checkParams( array('rule_id','status') ) )
            {
                $mRule = D('AuthRule');
                $row = $mRule->updataStatus( $params['rule_id'], $params['status'] );
                
                if( $row )
                    $this->ajaxResponse( 0, '修改成功！' );
                else
                    $this->ajaxResponse( 500, '修改失败！' );
            }
        }
        else
        {
            $mRule = D('AuthRule');
            $list = $mRule->getList( $params );

            # 实例化分页类 传入总记录数和每页显示的记录数
            $Page       = new Page( $list['page']['total'], $list['page']['size'] );
            $Page->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
            # 分页显示输出
            $show       = $Page->show();
            $this->pageContent = $show;
            $this->page = $list['page'];

            $this->lists = $list['value'];
            $this->display();
        }
    }

    # 添加规则
    public function add()
    {
        $params = $this->_params;
        if( IS_POST )
        {
            if( $this->checkParams( array('name', 'title') ) )
            {
                $mRule = D('AuthRule');
                $row = $mRule->addItem( $params );

                if( $row )
                    $this->ajaxResponse( 0, '添加成功！' );
                else
                    $this->ajaxResponse( 500, '添加失败，错误信息：' . $mRule->getDbError() );
            }
        }
        else
        {
            # 获取顶级规则
            $mRule = D('AuthRule');
            $list = $mRule->getList( array('pid'=>0) );

            // var_dump($list);

            $this->rules = $list['value'];
            $this->display();
        }
    }

    # 编辑规则
    public function edit()
    {
        $params = $this->_params;
        if( IS_POST )
        {
            if( $this->checkParams( array( 'id', 'name', 'title') ) )
            {
                $mRule = D('AuthRule');
                $row = $mRule->editItem( $params );

                if( $row )
                    $this->ajaxResponse( 0, '保存成功！' );
                else
                    $this->ajaxResponse( 500, '修改失败，错误信息：' . $mRule->getDbError() );
            }
        }
        else
        {
            if( $this->checkParams('id') )
            {
                # 获取顶级规则
                $mRule = D('AuthRule');
                $list = $mRule->getList( array('pid'=>0) );

                $detail = $mRule->getDetail( $params['id'] );

                $this->item = $detail;
                $this->rules = $list['value'];
                $this->display('add');
            }
        }
    }

}