<?php
namespace Adminis\Controller;
use Think\Page;

class HostController extends BaseController
{
	public function _initialize()
	{
		parent::_initialize();
	}

	public function index()
	{
		$params = $this->_params;

		$mHost = D('Host');
		$list = $mHost->getList( $params );
		
		# 实例化分页类 传入总记录数和每页显示的记录数
		$Page       = new Page( $list['page']['total'], $list['page']['size'] );
		$Page->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
		# 分页显示输出
		$show = $Page->show();
		$this->pageContent = $show;
		$this->page = $list['page'];

		$this->lists = $list['value'];
		$this->display();
	}

	# 删除域名
	public function removeHost()
	{
		if( IS_POST )
		{
			$params = $this->_params;
			if( $this->hasParams('id') )
			{
				$mHost = D( 'Host' );
				$row = $mHost->removeItem( $params['id'] );
				if( $row )
					$this->ajaxResponse( 0, '删除成功！' );
				else
					$this->ajaxResponse( 500, '删除失败，系统错误！' );
			}
			else
				$this->ajaxResponse( 500, '缺少参数：id' );
		}
	}

	# 获取信息
	public function detail()
	{
		if( IS_POST )
		{
			$params = $this->_params;
			if( $this->hasParams('id') )
			{
				$mHost = D( 'Host' );
				$detail = $mHost->getDetail( $params['id'] );

				if( $detail )
					$this->ajaxResponse( 0, 'success', $detail );
				else
					$this->ajaxResponse( 500, 'error' );
			}
			else
				$this->ajaxResponse( 500, '缺少参数：id' );
		}
	}
	
	# 编辑信息
	public function editDetail()
	{
		if( IS_POST )
		{
			$params = $this->_params;
			if( $this->hasParams('id') )
			{
				# 编辑
				$id = intval($params['id']);
				unset($params['id']);

				$mHost = D( 'Host' );
				$row = $mHost->editItem( $id, $params );

				if( $row )
					$this->ajaxResponse( 0, '保存成功！', $row );
				else
					$this->ajaxResponse( 500, '保存失败，服务器错误！' );
			}
			else
			{
				# 新增
				$mHost = D( 'Host' );
				$row = $mHost->addItem( $params );

				if( $row )
					$this->ajaxResponse( 0, '域名添加成功！', $row );
				else
					$this->ajaxResponse( 500, '您的域名已存在，请勿重复添加！' );
			}
		}
	}

}