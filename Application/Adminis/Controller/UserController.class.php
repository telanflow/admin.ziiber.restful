<?php
namespace Adminis\Controller;
use Think\Page;

class UserController extends BaseController
{

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        $params = $this->_params;

        if( IS_POST )
        {
            if( $this->checkParams( array('id','status') ) )
            {
                $mMember = D('member');
                $row = $mMember->updataStatus( $params['id'], $params['status'] );
                if( $row )
                    $this->ajaxResponse( 0, '修改成功！', $row );
                else
                    $this->ajaxResponse( 500, '修改失败！' );
            }
        }
        else
        {
            $mMember = D('member');
            $list = $mMember->getList( $params );
            
            $mGroup = D('AuthGroup');
            $groupList = $mGroup->getList();


            # 实例化分页类 传入总记录数和每页显示的记录数
            $Page       = new Page( $list['page']['total'], $list['page']['size'] );
            $Page->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
            # 分页显示输出
            $show       = $Page->show();
            $this->pageContent = $show;
            $this->page = $list['page'];
            
            $this->groupList = $groupList['value'];
            $this->lists = $list['value'];
            $this->display();
        }
    }

    # 设置用户组
    public function setGroup()
    {
        if( IS_POST )
        {
            if( $this->checkParams( array( 'uid', 'group_id' ) ) )
            {
                $params = $this->_params;

                $mGroupAccess = D('GroupAccess');
                $row = $mGroupAccess->setItem( $params['uid'], $params['group_id'] );

                if( $row )
                    $this->ajaxResponse( 0, '用户组设置成功！', $row );
                else
                    $this->ajaxResponse( 500, '用户组设置失败！' );
            }
        }
    }

}