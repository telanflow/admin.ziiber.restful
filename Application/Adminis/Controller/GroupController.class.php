<?php
namespace Adminis\Controller;
use Think\Page;

class GroupController extends BaseController
{

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        $params = $this->_params;

        $mGroup = D('AuthGroup');
        $list = $mGroup->getList( $params );

        # 实例化分页类 传入总记录数和每页显示的记录数
        $Page       = new Page( $list['page']['total'], $list['page']['size'] );
        $Page->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        # 分页显示输出
        $show       = $Page->show();
        $this->pageContent = $show;
        $this->page = $list['page'];
        
        $this->lists = $list['value'];
        $this->display();
    }

    public function rule()
    {
        $params = $this->_params;

        $mGroup = D('GroupAccess');
        if (IS_POST)
        {
            # 设置权限
            if ( false !== $mGroup->setRule( $params ) )
                $this->redirect('index');
            else
                $this->error( $mGroup->getError() );
        }
        else
        {
            if( $this->checkParams('id') )
            {
                $list = $mGroup->allRule();      #查询分组规则信息
                $row  = $mGroup->userrow( $params['id'] );   #查询当前要编辑的用户
                
                $this->list  = $list;
                $this->group = $row;
                $this->assign("rules", $row['rules']);

                $this->title = "角色列表";
                $this->display();
            }
        }

    }

}