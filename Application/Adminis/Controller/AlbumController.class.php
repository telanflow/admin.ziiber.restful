<?php
namespace Adminis\Controller;
use Think\Page;

class AlbumController extends BaseController
{
	public function _initialize()
	{
		parent::_initialize();
	}

	# 获取专辑列表
	public function index()
	{
		$params = $this->_params;

		if( empty($params['sort']) )
			$params['order'] = 'sort asc,create_time desc';
		else
			$params['order'] = $params['sort'] ? 'sort desc,create_time desc':'sort asc,create_time desc';

		$mAlbum = D('Album');
		$list = $mAlbum->getList( $params );
		
		# 实例化分页类 传入总记录数和每页显示的记录数
		$Page       = new Page( $list['page']['total'], $list['page']['size'] );
		$Page->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
		# 分页显示输出
		$show = $Page->show();
		$this->pageContent = $show;
		$this->page = $list['page'];

		$this->lists = $list['value'];
		$this->display();
	}

	# 获取专辑信息
	public function detail()
	{
		if( IS_POST )
		{
			$params = $this->_params;
			if( $this->hasParams('id') )
			{
				$mAlbum = D( 'Album' );
				$detail = $mAlbum->getDetail( $params['id'] );

				if( $detail )
					$this->ajaxResponse( 0, 'success', $detail );
				else
					$this->ajaxResponse( 500, 'error' );
			}
			else
				$this->ajaxResponse( 500, '缺少参数：id' );
		}
	}

	# 删除专辑
	public function removeDetail()
	{
		if( IS_POST )
		{
			$params = $this->_params;
			if( $this->hasParams('id') )
			{
				$mAlbum = D( 'Album' );
				$row = $mAlbum->removeItem( $params['id'] );
				if( $row )
					$this->ajaxResponse( 0, '删除成功！' );
				else
					$this->ajaxResponse( 500, '删除失败，系统错误！' );
			}
			else
				$this->ajaxResponse( 500, '缺少参数：id' );
		}
	}

	# 编辑专辑
	public function editDetail()
	{
		if( IS_POST )
		{
			$params = $this->_params;
			if( $this->hasParams('id') )
			{
				# 编辑
				$id = intval($params['id']);
				unset($params['id']);

				$mAlbum = D( 'Album' );
				$row = $mAlbum->editItem( $id, $params );

				if( $row )
					$this->ajaxResponse( 0, '专辑保存成功！', $row );
				else
					$this->ajaxResponse( 500, 'error' );
			}
			else
			{
				# 新增
				$mAlbum = D( 'Album' );
				$row = $mAlbum->addItem( $params );

				if( $row )
					$this->ajaxResponse( 0, '专辑添加成功！', $row );
				else
					$this->ajaxResponse( 500, 'error' );
			}
		}
	}

	# 排序
	public function sort()
	{
		if( IS_POST )
		{
			$params = $this->_params;

			$params = json_decode( $params['sort'], true );

			$mAlbum = D('Album');
			$mAlbum->editSort( $params );

			$this->ajaxResponse( 0, '操作成功！' );
		}	
	}

	# 设置默认专辑
	public function setDefault()
	{
		if( IS_POST )
		{
			if( $this->checkParams( 'id' ) )
			{
				$params = $this->_params;
				$Album = D('Album');
				$row = $Album->setDefault( intval($params['id']) );

				if( $row )
					$this->ajaxResponse( 0, '设置成功！', $row );
				else
					$this->ajaxResponse( 500, '修改失败，请重试！' );
			}
		}
	}

}