<?php
namespace Adminis\Controller;
use Think\Page;

class LogsController extends BaseController
{
    public function _initialize()
    {
        parent::_initialize();
    }

    # 获取日志列表
    public function index()
    {
        $params = $this->_params;
        
        $Logs = D('Logs');
        $list = $Logs->getList( $params );
        
        # 实例化分页类 传入总记录数和每页显示的记录数
        $Page       = new Page( $list['page']['total'], $list['page']['size'] );
        $Page->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        # 分页显示输出
        $show = $Page->show();
        $this->pageContent = $show;
        $this->page = $list['page'];

        $this->lists = $list['value'];
        $this->title = '系统日志';
        $this->display();
    }

}