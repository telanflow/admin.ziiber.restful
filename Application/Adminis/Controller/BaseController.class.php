<?php
namespace Adminis\Controller;
use Think\Controller;

class BaseController extends Controller
{
	protected $_session = null;
	protected $_params  = array();
    protected $_header  = array();
	protected $_headers  = array();

	public function _initialize()
	{
        $this->_headers = $this->getHeaders();

		# 1 用户如果没登录;
        if ( !session('?member') )
            $this->redirect('Member/login');

		$this->_session = session( 'member' );

        if( IS_GET )
            $this->_params = I( 'get.', null, false );
        elseif( IS_POST )
            $this->_params = I( 'post.', null, false );
        elseif( IS_PUT )
            $this->_params = I( 'put.', null, false );


		# 3 根据变量里面的当前登录者,判断是否具备当前页面的权限;
        if ( !authcheck( CONTROLLER_NAME . '/' . ACTION_NAME, intval($this->_session['id']) )
        	 and CONTROLLER_NAME != 'Index' )
        {
            if(true === $ajax || IS_AJAX)
                $this->ajaxResponse( 500, '你没有权限！' ); # AJAX提交
            else
                $this->error( '你没有权限！' ); # 没权就错误提示给用户;
        }
        
	}

	# 是否存在参数
	protected function hasParams( $key )
	{
		if( !isset($this->_params[$key]) )
            return false;
        else
            return true;
	}

	# 检查缺少的参数
	protected function checkParams( $keys = array() )
	{
        if( is_array($keys) )
            foreach ($keys as $key)
            {
                if( !$this->hasParams($key) )
                {
                    $this->lackofParams( $key );
                    return false;
                }
            }
        else
            if( !$this->hasParams($keys) )
            {
                $this->lackofParams( $keys );
                return false;
            }

        return true;
	}

	/**
     * 缺少字段的返回
     * @param  string $key 缺少的字段名
     * @return json
     */
    protected function lackofParams( $key )
    {
        $this->ajaxResponse( 500, 'invalid ' . $key, array() );
    }

	# ajax返回
	protected function ajaxResponse( $code, $message, $value = array(), $type = 'json' )
	{
		if( in_array( $code, array(404, 403, 500, 502) ) )
			$this->setHeader( $code );
		else
			$this->setHeader( 200 );

		# 添加 Header
		$this->setHeaderItem( 'x-powered-by', 'ziiber.me' );
		$this->setHeaderItem( 'x-api-version', '1.0.1' );

		$ret = array(
			'code' => $code,
			'message' => $message,
			'value' => $value
		);

		# 把 Header 设置到Http协议上
		$this->sendHeader();

		$this->ajaxReturn( $ret, $type );
		exit();
	}

	# 设置http返回状态
	protected function sendHeader()
	{
		foreach ($this->_header as $value)
			header( $value );
	}

	# 添加header 状态到 $this->_header
	protected function setHeader( $code )
	{
		$status = array(
			// Informational 1xx
			100 => 'Continue',
			101 => 'Switching Protocols',
			// Success 2xx
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			// Redirection 3xx
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Moved Temporarily ',  // 1.1
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			// 306 is deprecated but reserved
			307 => 'Temporary Redirect',
			// Client Error 4xx
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			// Server Error 5xx
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported',
			509 => 'Bandwidth Limit Exceeded'
		);
		
		if( array_key_exists( $code, $status ) )
			$this->_header[] = 'HTTP/1.1 '.$code.' '.$status[$code];
	}

	# 添加Header
	protected function setHeaderItem( $key, $value )
	{
		$this->_header[] = ucwords(strtolower($key)) . ':' . $value;
	}

    # 获取Header头信息
    protected function getHeaders()
    {
        $server = I('server.');
        $header = array();

        foreach ($server as $key => $value)
        {
            if( 'HTTP_' === substr( $key, 0, 5 ) )
            {
                $str = substr( $key, 5 );
                $str = strtolower($str);
                $header[$str] = $value;
            }
        }

        return $header;
    }

    # Header是否存在
    protected function hasHeaders( $key )
    {
        if( !isset($this->_headers[$key]) or empty($this->_headers[$key]) )
            return false;
        else
            return true;
    }

    # 检查缺少的Headers
    protected function checkHeaders( $keys = array() )
    {
        if( is_array($keys) )
        {
            foreach ($keys as $key)
            {
                if( !$this->hasHeaders($key) )
                {
                    $this->lackofHeaders( $key );
                    return false;
                }
            }
        }
        else
        {
            if( !$this->hasHeaders($keys) )
            {
                $this->lackofHeaders( $keys );
                return false;
            }
        }

        return true;
    }

    # 缺少Header的返回
    protected function lackofHeaders( $key )
    {
        # Illegal request
        $this->ajaxResponse( 500, 'invalid ' . $key, array() );
    }
    
}