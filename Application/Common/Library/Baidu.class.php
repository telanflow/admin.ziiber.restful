<?php
namespace Common\Library;

use Common\Library;
class Baidu extends Base{

	protected $inlay = array();
	
    public function __construct()
    {
    	parent::__construct();
    	$this->inlay = unserialize( F( 'baidu' ) );

    	$this->getBaiduId();
    }

    public function __destruct()
    {
    	F( 'baidu', serialize($this->inlay) );
    }

    # 获取百度ID
	public function getBaiduId()
	{
		if( empty($this->inlay['BAIDUID']) )
		{
			$this->_header['nobody'] = true;
			$this->_header['header'] = true;

			$result = http( 'http://fm.baidu.com', null, null, $this->_header );
			preg_match("/set\-cookie:([^\r\n]*)/i", $result['content'], $matches); 
			
			preg_match('/BAIDUID=(\S*)\;/i', $matches[1], $cookies);

			if( count($cookies) > 1 )
			{
				$this->inlay['BAIDUID'] = $cookies[1];
				return $cookies[1];
			}
			else
		        return false;
		}
		else
			return $this->inlay['BAIDUID'];
	}

	# 获取歌曲信息
	public function getDetailById( $songId = null )
	{
		$url = 'http://ting.baidu.com/data/music/links?songIds=877578';
		$result = $this->_request();
	}

	public function lists( $params = array() )
	{
		$number = 100;


	}
	
	# 获取分类列表
	public function getTypeList()
	{
		$url = 'http://fm.baidu.com/dev/api/?tn=channellist&hashcode=' . $this->inlay['hashcode'] . '&_='.time();
		
		$result = $this->_request( $url );
		
		if( $result )
			return $result;
		else
			return false;
	}

	# 获取场景列表
	public function getScence()
	{
		$url = 'http://music.baidu.com/data/scene/list?hashcode=' . $this->inlay['hashcode'] . '&_=' . time();
		$result = $this->_request( $url );

		if( $result['content'] )
			$row = json_decode( $result, true );
		else
			return false;
	}

	protected function _request( $url = '', $params = null, $method = 'get' )
    {
    	$this->_header['cookie'] = $this->inlay;
        $result = http( $url, $params, $method, $this->_header );
        
        $ret = json_decode( $result['content'], true);

        if( isset($ret['hashcode']) )
	        $this->inlay['hashcode'] = $ret['hashcode'];

        return $ret;
    }

    # 搜索歌曲
    public function search()
    {
    	$url = 'http://brisk.eu.org/api/song.php?name=海阔天空';

    	/*
    	format: json|xml|jsonp
		callback: 若返回 jsonp 格式，请设置一个callback方法名
		query: 关键词
		_: 时间戳
		*/
    	$url = 'http://tingapi.ting.baidu.com/v1/restserver/ting?from=webapp_music&method=baidu.ting.search.catalogSug&format=json&callback&query=海阔天空&_=1413017198449';
    	

		$url = 'http://tingapi.ting.baidu.com/v1/restserver/ting?from=qianqian&version=2.1.0&method=baidu.ting.search.common&format=json&query=海阔天空&page_no=1&page_size=30';
    }

}