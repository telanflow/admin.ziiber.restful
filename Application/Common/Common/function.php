<?php

/**
 * HTTP请求
 * @author ziiber <ziiber@foxmail.com>
 * @param  string $url    url
 * @param  array  $params 参数
 * @param  string $method 请求方式 get post put
 * @param  array  $header 协议头
 * @return [type]         [description]
 */
function http( $url = '', $params = array(), $method = 'get', $header = array() )
{
	$ret    = null;
	$result = null;
	$method = strtolower($method);

	# 初始化curl
	$ch = curl_init();

	# Default Option
	curl_setopt($ch, CURLOPT_FAILONERROR, true); 	# 显示HTTP状态码
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); # curl_exec 不直接输出到浏览器
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);	# 301重定向
	// curl_setopt($ch, CURLOPT_HEADER, true);			# 设置头信息返回

	# 请求类型
	if( $method == 'post' )
	{
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $params );
	}
	elseif( $method == 'put' )
	{
		curl_setopt($ch, CURLOPT_PUT, true);
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $params );
	}
	elseif( $method == 'get' )
	{
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		if( !empty($params) )
		{
			$link = array();
			foreach ($params as $key => $value)
				$link[] = $key . '=' . $value;
			$url .= '?'.implode('&', $link);
		}
	}

	# 设置url	
	curl_setopt($ch, CURLOPT_URL, $url);

	# 一个用来设置HTTP头字段的数组。
	# 使用如下的形式的数组进行设置： array('Content-type: text/plain', 'Content-length: 100') 
	if( !is_assoc($header) )
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

	# 启用时将不对HTML中的BODY部分进行输出。
	if( isset($header['nobody']) )
		curl_setopt($ch, CURLOPT_NOBODY, $header['nobody']);
	else
		curl_setopt($ch, CURLOPT_NOBODY, false);

	# 启用时会将头文件的信息作为数据流输出
	if( isset($header['header']) )
		curl_setopt($ch, CURLOPT_HEADER, $header['header']);
	else
		curl_setopt($ch, CURLOPT_HEADER, false);

	# User_Agent
	if( isset($header['useragent']) )
		curl_setopt($ch, CURLOPT_USERAGENT, $header['useragent']);

	# Cookie
	# 多个cookie用分号分隔，分号后带一个空格(例如， "fruit=apple; colour=red")
	if( isset($header['cookie']) )
	{
		if( is_array($header['cookie']) )
		{
			$cookies = array();
			foreach ($header['cookie'] as $key => $value)
				$cookies[] = $key . '=' . $value;
			
			curl_setopt($ch, CURLOPT_COOKIE, implode('; ', $cookies));
		}
		else
			curl_setopt($ch, CURLOPT_COOKIE, $header['cookie']);
	}

	# 读取文件中的cookie
	if( isset($params['cookiefile']) )
		curl_setopt($ch, CURLOPT_COOKIEFILE, $params['cookiefile']);
	
	# 保存cookie到文件
	if( isset($params['cookiejar']) )
		curl_setopt($ch, CURLOPT_COOKIEJAR, $params['cookiejar']);

	# Referer
	if( isset($header['referer']) )
		curl_setopt($ch, CURLOPT_REFERER, $header['referer']);
	else
		curl_setopt($ch, CURLOPT_AUTOREFERER, true); # 自动设置Referer

	# Encoding
	# HTTP请求头中"Accept-Encoding: "的值。
	# 支持的编码有"identity"，"deflate"和"gzip"。如果为空字符串""，请求头会发送所有支持的编码类型。 
	if( isset($header['encoding']) )
		curl_setopt($ch, CURLOPT_ENCODING, $header['encoding']);

	# HTTP代理通道
	# 一个用来连接到代理的"[username]:[password]"格式的字符串
	if( isset($header['proxy']) )
	{
		curl_setopt($ch, CURLOPT_PROXY, true);
		curl_setopt($ch, CURLOPT_PROXYUSERPWD, $header['proxy']);
	}

	# 发送请求
	$result = curl_exec($ch);

	if( $result !== false )
	{
		$info = curl_getinfo($ch);
		
		$ret  = array(
			'content' =>	$result,
			'info'	  =>	$info
		);
	}
	else
		$ret = false;
	
	# 关闭curl
	curl_close($ch);

	return $ret;
}


function is_assoc($arr) {
    return array_keys($arr) !== range(0, count($arr) - 1);  
}  

# 获取专辑名称
function getAlbumName( $id )
{
	$name = M('album')->where( array('id'=>$id) )->getField( 'title' );
	echo $name;
}

# 获取用户昵称
function getMemberName( $id )
{
	$name = M('member')->where( array('id'=>$id) )->getField( 'nickname' );
	return $name;
}

function getGroupId( $uid )
{
	$Group = D('GroupAccess');
	$group_id = $Group->getGroup($uid);
	return $group_id;
}

/*
 * 权限认证
 * @param $rule 要验证的规则名称；
 * @param $uid 用户的id；
 * @param $relation 规则组合方式，默认为‘or’，以上三个参数都是根据Auth的check（）函数来的，
 */

function authcheck($rule, $uid, $type = 1, $relation = 'or', $mode = 'url')
{
    //判断当前用户UID是否在定义的超级管理员参数里
    if ($uid == 0)
        return true;
    else
    {
        $auth = new \Think\Auth();
        return $auth->check( $rule, $uid, $type, $mode, $relation ) ? true : false;
    }
}

# 获取IP归属地
function getIpAddress( $ip )
{
	// var_dump( ip2long('115.197.141.248') );
	$Ip = new Org\Net\IpLocation('UTFWry.dat'); // 实例化类 参数表示IP地址库文件
	$address = $Ip->getlocation( $ip ); // 获取某个IP地址所在的位置
	// var_dump($address);
	return $address['country'];
}

function getUaMsg( $User_Agent )
{
	$msg = '';
	$msg .= '<span class="text-warning">' . determineplatform( $User_Agent ) . '</span>  ';
	$msg .= '<span class="text-success">' . getBrowserCore( $User_Agent ) . '</span>';
	return $msg;
}

//获取浏览器内核引擎
function getBrowserCore($Agent)
{
	$browseragent="";   //浏览器
	$browserversion=""; //浏览器的版本
	
	//判断浏览器
	if( eregi('UCBrowser/([0-9\.]+)',$Agent,$version) ){
		$browserversion=$version[1];
		stripos($Agent,'Mobile')!==false ? $browseragent="UC浏览器(移动版)" : $browseragent="UC浏览器";
	}
	elseif( eregi('LBBROWSER',$Agent) ){
		$browseragent='猎豹浏览器';
	}
	elseif( eregi('LieBaoFast/([0-9\.]+)',$Agent,$version) ){
		$browserversion=$version[1];
		stripos($Agent,'Mobile')!==false ? $browseragent='猎豹浏览器(移动版)' : $browseragent='猎豹浏览器';
	}
	elseif( eregi('QQBrowser/([0-9\.]+)',$Agent,$version) ){
		$browserversion=$version[1];
		stripos($Agent,'Mobile')!==false && stripos($Agent,'MQQBrowser')!==false ? $browseragent='QQ浏览器(移动版)' : $browseragent='QQ浏览器';
		if( stripos($Agent,'MicroMessenger') ){ $browseragent='微信';unset($browserversion); }
	}
	elseif( ereg('SE 2.X MetaSr 1.0',$Agent) ){
		stripos($Agent,'Chrome') !==false ? $browseragent='搜狗浏览器(极速)' : $browseragent='搜狗浏览器(兼容)';
	}
	elseif( eregi('SogouMobileBrowser/([0-9\.]+)',$Agent,$version) ){
		$browserversion=$version[1];
		stripos($Agent,'Mobile')!==false ? $browseragent='搜狗浏览器(移动版)' : $browseragent='搜狗浏览器';
	}
	elseif( eregi('baidubrowser/([0-9\.]+)',$Agent,$version) ){
		$browserversion=$version[1];
		stripos($Agent,'Mobile')!==false ? $browseragent='百度浏览器(移动版)' : $browseragent='百度浏览器';
	}
	elseif( eregi('Maxthon/([0-9\.]+)',$Agent,$version) ){
		$browserversion=$version[1];
		$browseragent='傲游浏览器';
	}
	elseif( eregi('MxBrowser/([0-9\.]+)',$Agent,$version) ){
		$browserversion=$version[1];
		stripos($Agent,'Mobile')!==false ? $browseragent='傲游浏览器(移动版)' : $browseragent='傲游浏览器';
	}
	elseif( eregi('Mb2345Browser/([0-9\.]+)',$Agent,$version) ){
		$browserversion=$version[1];
		$browseragent='2345浏览器';
	}
	elseif( eregi('The.?World',$Agent) ){
		//$browserversion=$version[1];
		$browseragent='世界之窗浏览器';
	}
	elseif( eregi('360SE',$Agent) ){
		//$browserversion=$version[1];
		$browseragent='360安全浏览器';
	}
	elseif( eregi('360 Aphone Browser \(([0-9\.]+)\)',$Agent,$version) ){
		$browserversion=$version[1];
		$browseragent='360手机浏览器';
	}
	elseif( eregi('2345Explorer ([0-9\.]+))',$Agent,$version) ){
		$browserversion=$version[1];
		$browseragent='2345王牌浏览器';
	}
	elseif( eregi('2345chrome v([0-9\.]+))',$Agent,$version) ){
		$browserversion=$version[1];
		$browseragent='2345极速浏览器';
	}
	//判断内核
	elseif( eregi('MSIE ([5-9]\.[0-9]{1,2})',$Agent,$version) ){
		$browserversion=$version[1];
		$browseragent="Internet Explorer";
	}
	elseif( eregi('Trident/',$Agent) && eregi('rv:([0-9\.]+)',$Agent,$version)){
		$browserversion=$version[1];
		$browseragent="Internet Explorer";
	}
	
	elseif( eregi( 'Opera/([0-9]{1,3}.[0-9]{1,3})',$Agent,$version) ){
		$browserversion=$version[1];
		ereg('Opera Mini/([0-9\.]+)',$Agent,$version) && $browserversion=$version[1];
		$browseragent="Opera";
	}
	elseif( eregi( 'Firefox/([0-9\.]+)',$Agent,$version) ){
		$browserversion=$version[1];
		$browseragent="Firefox";
	}
	elseif( eregi( 'Chrome/([0-9\.]+)',$Agent,$version) ){
		$browserversion=$version[1];
		$browseragent="Chrome";
	}
	elseif( eregi( 'Safari/([0-9\.]+)',$Agent,$version) ){
		$browseragent="Safari";
		$browserversion=$version[1];
	}
	else{
		$browserversion="";
		$browseragent="Unknown";
	}
	$browserversion && $browserversion='|'.$browserversion;
	return $browseragent.$browserversion;
}

//获取操作系统
function determineplatform( $Agent )
{
	$browserplatform = '';
	$arr = array();
	if (eregi('win',$Agent) && strpos($Agent, '95')){
		$browserplatform="Windows 95";
	}
	elseif (eregi('win 9x',$Agent) && strpos($Agent, '4.90')){
		$browserplatform="Windows ME";
	}
	elseif (eregi('win',$Agent) && ereg('98',$Agent)){
		$browserplatform="Windows 98";
	}
	elseif ( eregi('Win',$Agent) && eregi('nt ([0-9\.]+)',$Agent,$arr) )
	{
		switch( $arr[1] )
		{
			case '5.0':$browserplatform="Windows 2000";break;
			case '5.1':$browserplatform="Windows XP";break;
			case '5.2':$browserplatform="Windows Server 2003";break;
			case '6.0':$browserplatform="Windows Vista";break;
			case '6.1':$browserplatform="Windows 7";break;
			case '6.2':$browserplatform="Windows 8";break;
			case '6.3':$browserplatform="Windows 8.1";break;
			case '6.4':$browserplatform="Windows 10";break;
			case '10.0':$browserplatform="Windows 10";break;
		}
	}
	elseif( eregi('win',$Agent) && ereg('32',$Agent))
	{
		$browserplatform="Windows 32";
	}
	elseif( eregi('win',$Agent) && ereg('WOW64',$Agent))
	{
		$browserplatform="Windows 64";
	}
	elseif( eregi('win',$Agent) && eregi('nt',$Agent))
	{
		$browserplatform="Windows NT";
	}
	elseif( ereg('Mobile',$Agent))
	{
		//移动设备
		if(ereg('iPhone',$Agent)){
			$browserplatform="iPhone";
		}
		elseif(ereg('Android ([0-9\.]+)',$Agent,$arr)){
			$browserplatform="Android".$arr[1];
		}
		elseif(ereg('iPad',$Agent)){
			$browserplatform="iPad";
		}

	}
	elseif( stripos($Agent,'Android') !== false )
	{
		$browserplatform="Android";
	}
	elseif( eregi('Mac OS',$Agent) )
	{
		$browserplatform="Mac OS";
	}
	elseif( eregi('linux',$Agent) )
	{
		$browserplatform="Linux";
	}
	elseif( eregi('unix',$Agent) )
	{
		$browserplatform="Unix";
	}
	elseif( eregi('sun',$Agent) && eregi('os',$Agent) )
	{
		$browserplatform="SunOS";
	}
	elseif( eregi('ibm',$Agent) && eregi('os',$Agent) )
	{
		$browserplatform="IBM OS/2";
	}
	elseif( eregi('Mac',$Agent) && eregi('PC',$Agent) )
	{
		$browserplatform="Macintosh";
	}
	elseif( eregi('PowerPC',$Agent) ){
		$browserplatform="PowerPC";
	}
	elseif( eregi('AIX',$Agent) ){
		$browserplatform="AIX";
	}
	elseif( eregi('HPUX',$Agent) ){
		$browserplatform="HPUX";
	}
	elseif( eregi('NetBSD',$Agent) ){
		$browserplatform="NetBSD";
	}
	elseif( eregi('BSD',$Agent) ){
		$browserplatform="BSD";
	}
	elseif( eregi('OSF1',$Agent) ){
		$browserplatform="OSF1";
	}
	elseif( eregi('IRIX',$Agent) ){
		$browserplatform="IRIX";
	}
	elseif( eregi('FreeBSD',$Agent) ){
		$browserplatform="FreeBSD";
	}
	if ( $browserplatform=='' ){
		$browserplatform = "Unknown";
	}
	return $browserplatform;
}
