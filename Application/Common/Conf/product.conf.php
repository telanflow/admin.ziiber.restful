<?php
/**
 * 线上服务器配置文件
 * @author Ziiber <ziiber@foxmail.com>
 * @copyright 2015.07.15
 * @version 1.0.0
 */

$product = array(
	# 数据库配置
    'DB_HOST'   => '127.0.0.1', // 服务器地址(Host)
    'DB_NAME'   => 'ziiber_music',                            // 数据库名(DB Name)
    'DB_USER'   => 'root',                             // 用户名(Username)
    'DB_PWD'    => '123456',                           // 密码(Password)
    'DB_PORT'   => '3306',                                       // 端口(Port)
    'DB_PREFIX' => 'mc_',                                      // 数据库表前缀

    # 路由配置
    'URL_ROUTER_ON'   => true, 							// 开启路由
    'URL_ROUTE_RULES' => array(
	    # 'baidu/:' => array('Api/Index/query', 'status=1'),
	),

    # 域名配置
    'APP_SUB_DOMAIN_DEPLOY'   =>    1, # 开启子域名配置
    'APP_SUB_DOMAIN_RULES'    =>    array(   
        'api.ziiber.me'  => 'Api',
        'admin.ziiber.me'  => 'Adminis'
    ),

    # Session
    'SESSION_TYPE'   => 'Db', // session hander类型
    'SESSION_PREFIX' => 'thk_', // session 前缀
    'SESSION_EXPIRE' => 3600, //session 过期时间
    'SESSION_TABLE'  => 'mc_session', //存放session 数据表

);